//
//  MainViewController.h
//  Meat Calculator
//
//  Created by Lenny Khazan on 12/2/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FlipsideViewController.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource> {
    IBOutlet UILabel *ovenTempLabel;
    IBOutlet UILabel *internalTempLabel;
    IBOutlet UILabel *timeLabel;
    IBOutlet UITextField *weight;
    IBOutlet UIPickerView *meatPicker;
    
    UIButton *backgroundBtn;
    
    LKMeat *meat;
    
    LKMeat *currentMeatSelection;
    LKMeat *currentSubMeatSelection;
    
    //The flipside view controller. We will use this to pre-load the webpage.
    FlipsideViewController *flipside;
    
    //iPad only controls
    IBOutlet NSTimer* timer;
    IBOutlet UIProgressView *progressView;
    IBOutlet UILabel *remainingTimeLabel;
    IBOutlet UILabel *restTimeLabel;
    IBOutlet UILabel *cookMethod;
    IBOutlet UIButton *startTimeButton;
    int minRemaining;
    int cookTime;
    
    //minuteLabel is used to change the "minutes" label to "minute" when there is 1 minute remaining on the iPad.
    IBOutlet UILabel *minuteLabel;
    
    //Initialize some basic calculate blocks.
    float (^calcPerLb)(float cTime, float weight);
    float (^calcTotal)(float cTime, float weight);
    float (^calcPerSide)(float cTime, float weight);
    float (^calcNone)(float cTime, float weight);
}

- (IBAction)showInfo:(id)sender;
- (IBAction)calculate:(id)sender;
- (IBAction)hideKeyboard:(id)sender;
- (IBAction)showBgBtn:(id)sender;

//iPad Only
- (IBAction)timerstart:(id)sender;
- (IBAction)timerAction:(id)sender;


@property (strong, nonatomic) UIPopoverController *flipsidePopoverController;
@property (nonatomic, retain) NSTimer *timer;
@property (nonatomic, retain) UIProgressView *progressView;
@property (nonatomic, retain) UILabel *remainingTimeLabel;
@property (nonatomic, retain) UILabel *ovenTempLabel;
@property (nonatomic, retain) UILabel *internalTempLabel;
@property (nonatomic, retain) UILabel *timeLabel;
@property (nonatomic, retain) UITextField *weight;
@property (nonatomic, retain) UIPickerView *meatPicker;
@property (nonatomic, retain) UIButton *backgroundBtn;
@property (nonatomic, retain) LKMeat *meat;
@property (nonatomic, retain) LKMeat *currentMeatSelection;
@property (nonatomic, retain) LKMeat *currentSubMeatSelection;
@property (nonatomic, retain) FlipsideViewController *flipside;
@property (nonatomic, readwrite) int minRemaining;
@property (nonatomic, retain) UILabel *minuteLabel;
@property (nonatomic, retain) UILabel *restTimeLabel;
@property (nonatomic, retain) UIButton *startTimeButton;

@end
