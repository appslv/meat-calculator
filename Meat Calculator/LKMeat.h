//
//  LKMeat.h
//  Meat Calculator
//
//  Created by Lenny Khazan on 12/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LKMeat : NSObject {
    NSArray *subMeats;
    NSString *meat;
    float cookTime;
    float cookTemp;
    float internalTemp;
    float restTime;
    NSString *cookMethod;
    float (^calcTime)(float cTime, float weight);
}

-(id)initWithName:(NSString *)name subMeats:(NSArray *)sub cookTempF:(float)cTemp internalTempF:(float)iTemp restTime:(float)rTime cookMethod:(NSString *)cMethod cookTimeCalculation:(float(^)(float cTime, float weight))calcTimeBlock cookTime:(float)cTime;
-(id)initWithName:(NSString *)name;
-(id)initWithName:(NSString *)name subMeats:(NSArray *)sub;

@property (nonatomic, retain) NSArray *subMeats;
@property (nonatomic, retain) NSString *meat;
@property (nonatomic, retain) NSString *cookMethod;
@property (nonatomic, readwrite) float cookTemp;
@property (nonatomic, readwrite) float internalTemp;
@property (nonatomic, readwrite) float restTime;
@property (nonatomic, readwrite) float cookTime;
@property (nonatomic, readonly) float (^calcTime)(float cTime, float weight);

@end
