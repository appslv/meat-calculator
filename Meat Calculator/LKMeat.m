//
//  LKMeat.m
//  Meat Calculator
//
//  Created by Lenny Khazan on 12/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "LKMeat.h"

@implementation LKMeat

@synthesize meat, subMeats, internalTemp, cookTemp, cookMethod, restTime, calcTime, cookTime;

-(id)init {
    self = [super init];
    if (self) {
        meat = nil;
        subMeats = nil;
        calcTime = nil;
        cookTemp = 0;
        internalTemp = 0;
        restTime = 0;
        cookMethod = 0;
        cookTime = 0;
    }
    return self;
}

-(id)initWithName:(NSString *)name subMeats:(NSArray *)sub {
    self = [super init];
    if (self) {
        meat = name;
        subMeats = sub;
        calcTime = nil;
        cookTemp = 0;
        internalTemp = 0;
        restTime = 0;
        cookMethod = 0;
        cookTime = 0;
    }
    return self;
}

-(id)initWithName:(NSString *)name {        
    self = [super init];
    if (self) {
        meat = name;
        subMeats = nil;
        cookTemp = 0;
        calcTime = nil;
        internalTemp = 0;
        restTime = 0;
        cookMethod = 0;
        cookTime = 0;
    }
    return self;
}

-(id)initWithName:(NSString *)name subMeats:(NSArray *)sub cookTempF:(float)cTemp internalTempF:(float)iTemp restTime:(float)rTime cookMethod:(NSString *)cMethod cookTimeCalculation:(float(^)(float cTime, float weight))calcTimeBlock cookTime:(float)cTime {
    self = [super init];
    if (self) {
        meat = name;
        subMeats = sub;
        calcTime = calcTimeBlock;    
        cookTemp = cTemp;
        internalTemp = iTemp;
        restTime = rTime;
        cookMethod = cMethod;
        cookTime = cTime;
    }
    return self;
}

@end
