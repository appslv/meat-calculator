//
//  MainViewController.m
//  Meat Calculator
//
//  Created by Lenny Khazan on 12/2/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"
#import <AVFoundation/AVAudioPlayer.h>

@implementation MainViewController

@synthesize flipsidePopoverController = _flipsidePopoverController;
@synthesize timer, progressView, remainingTimeLabel, ovenTempLabel, internalTempLabel, timeLabel, weight, meatPicker, backgroundBtn, meat,currentMeatSelection, currentSubMeatSelection, flipside, minRemaining, minuteLabel, restTimeLabel, startTimeButton;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)hideKeyboard:(id)sender {
    //NOTE: Return key isn't displayed in number keyboard. Therefore, background button will appear when keyboard is displayed, then dissappear when pressed.
    [weight resignFirstResponder];
    [backgroundBtn removeFromSuperview];
}

- (IBAction)showBgBtn:(id)sender {
    [self.view addSubview:backgroundBtn];
}

- (IBAction)calculate:(id)sender {
    float weightFloat;
    @try {
        weightFloat = [weight.text floatValue];
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The weight you entered isn't a valid number." delegate:nil cancelButtonTitle:@"I'll fix it..." otherButtonTitles:nil];
        [alert show];
        return;
    }
    @finally {
        ovenTempLabel.text = (int)currentSubMeatSelection.cookTemp?[NSString stringWithFormat:@"%1.0f", currentSubMeatSelection.cookTemp]:@"--";
        cookTime = (currentSubMeatSelection.calcTime(currentSubMeatSelection.cookTime, weightFloat));
        timeLabel.text = (int)cookTime?[NSString stringWithFormat:@"%d", cookTime]:@"--";
        internalTempLabel.text = (int)currentSubMeatSelection.internalTemp?[NSString stringWithFormat:@"%1.0f", (currentSubMeatSelection.internalTemp)]:@"--";
        cookMethod.text = (int)currentSubMeatSelection.cookMethod?currentSubMeatSelection.cookMethod:@"--";
        restTimeLabel.text = (int)currentSubMeatSelection.restTime?[[NSString alloc] initWithFormat:@"%1.0f", currentSubMeatSelection.restTime]:@"--";
        remainingTimeLabel.text = timeLabel.text;
        
        startTimeButton.enabled = TRUE;
        startTimeButton.alpha = 1.0;
        [startTimeButton setTitle:@"Start" forState:UIControlStateNormal];
        [timer invalidate];
        timer = nil;
        progressView.progress = 0.0;
    }
    
        
}

#pragma mark - UIPickerViewDelegate / UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 2;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *retval = (id)view;
    if (!retval) {
        retval= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)];
    }
    if (component == 0) {
        retval.text = [[meat.subMeats objectAtIndex:row] meat];
    } else {
        retval.text = [[currentMeatSelection.subMeats objectAtIndex:row] meat];
    }
    retval.font = [UIFont systemFontOfSize:15];
    return retval;
}


-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if (component == 0) {
            return 180;
        } else {
            return 300;
        }
    } else {
        return self.view.frame.size.height / 2;    }
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        NSLog(@"Meat submeat count = %d", [meat.subMeats count]);
        return [meat.subMeats count];
    } else {
        if (currentMeatSelection) {
            return [currentMeatSelection.subMeats count];
        } else {
            return 0;
        }
    }
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        NSLog(@"Submeat for row: %d\nMeat:%@", row, [meat.subMeats objectAtIndex:row]);
        NSString *title = [[meat.subMeats objectAtIndex:row] meat];
        return title;
    } else {
        return [[currentMeatSelection.subMeats objectAtIndex:row] meat];
    }
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        LKMeat *newSelection = [meat.subMeats objectAtIndex:row];
        NSLog(@"New Meat Selection: %@", newSelection.meat);
        currentMeatSelection = newSelection;
        [thePickerView reloadComponent:1];
    } else {
        currentSubMeatSelection = [currentMeatSelection.subMeats objectAtIndex:row];
        NSLog(@"New Sub-meat Selection: %@", currentSubMeatSelection.meat);
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    backgroundBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backgroundBtn.frame = self.view.frame;
    [backgroundBtn addTarget:self action:@selector(hideKeyboard:) forControlEvents:UIControlEventTouchUpInside];
    
    //Old meat values (not gov-approved)
    /*
    //Create LKMeat objects for every meat/meat subtype, and then one to hold ALL meats.
    //We need to start at the lowest subMeat level. Here, we initialize LKMeat objects for beef sub-meats.
    LKMeat *beefRare = [[LKMeat alloc] initWithName:@"Rare" subMeats:nil cookTimePerLb:25 cookTempF:325 internalTempF:0];
    LKMeat *beefMedium = [[LKMeat alloc] initWithName:@"Medium" subMeats:nil cookTimePerLb:30 cookTempF:325 internalTempF:0];
    LKMeat *beefWell = [[LKMeat alloc] initWithName:@"Well Done" subMeats:nil cookTimePerLb:35 cookTempF:325 internalTempF:0];
    
    LKMeat *chickenNoStuff = [[LKMeat alloc] initWithName:@"Not Stuffed" subMeats:nil cookTimePerLb:40 cookTempF:325 internalTempF:0];
    LKMeat *chickenStuff = [[LKMeat alloc] initWithName:@"Stuffed" subMeats:nil cookTimePerLb:30 cookTempF:325 internalTempF:0];
    
    LKMeat *turkey8to12 = [[LKMeat alloc] initWithName:@"8-12 Lb" subMeats:nil cookTimePerLb:20 cookTempF:325 internalTempF:0];
    LKMeat *turkey18to20 = [[LKMeat alloc] initWithName:@"18-20 Lb" subMeats:nil cookTimePerLb:14 cookTempF:325 internalTempF:0];
    
    LKMeat *freshRib = [[LKMeat alloc] initWithName:@"Rib and Loin" subMeats:nil cookTimePerLb:40 cookTempF:325 internalTempF:0];
    LKMeat *freshLeg = [[LKMeat alloc] initWithName:@"Leg" subMeats:nil cookTimePerLb:30 cookTempF:325 internalTempF:0];
    LKMeat *freshPicnic = [[LKMeat alloc] initWithName:@"Picnic Shoulder" subMeats:nil cookTimePerLb:40 cookTempF:325 internalTempF:0];
    LKMeat *freshShoulderButt = [[LKMeat alloc] initWithName:@"Shoulder/Butt" subMeats:nil cookTimePerLb:50 cookTempF:325 internalTempF:0];
    LKMeat *freshBonedRolledShoulder = [[LKMeat alloc] initWithName:@"Boned/Rolled Shoulder" subMeats:nil cookTimePerLb:60 cookTempF:325 internalTempF:0];
    
    LKMeat *smokedShoulderPicnicHam = [[LKMeat alloc] initWithName:@"Shoulder/Picnic Hams" subMeats:nil cookTimePerLb:40 cookTempF:325 internalTempF:0];
    LKMeat *smokedBonelessButt2 = [[LKMeat alloc] initWithName:@"Boneless Butt (2 Lb)" subMeats:nil cookTimePerLb:40 cookTempF:325 internalTempF:0];
    LKMeat *smokedBonelessButt4 = [[LKMeat alloc] initWithName:@"Boneless Butt (4 Lb)" subMeats:nil cookTimePerLb:25 cookTempF:325 internalTempF:0];
    LKMeat *smokedHam12to20 = [[LKMeat alloc] initWithName:@"Ham (12-20 Lb)" subMeats:nil cookTimePerLb:18 cookTempF:325 internalTempF:0];
    LKMeat *smokedHam10 = [[LKMeat alloc] initWithName:@"Ham (<10 Lb)" subMeats:nil cookTimePerLb:20 cookTempF:325 internalTempF:0];
    LKMeat *smokedHalfHam = [[LKMeat alloc] initWithName:@"Half Ham" subMeats:nil cookTimePerLb:25 cookTempF:325 internalTempF:0];
    
    
    //Initialize an LKMeat object for different kinds of meat.
    beef = [[LKMeat alloc] initWithName:@"Beef" subMeats:[NSArray arrayWithObjects:beefRare, beefMedium, beefWell, nil]];
    chicken = [[LKMeat alloc] initWithName:@"Chicken" subMeats:[NSArray arrayWithObjects:chickenNoStuff, chickenStuff, nil]];
    turkey = [[LKMeat alloc] initWithName:@"Turkey" subMeats:[NSArray arrayWithObjects:turkey8to12, turkey18to20, nil]];
    freshPork = [[LKMeat alloc] initWithName:@"Fresh Pork" subMeats:[NSArray arrayWithObjects:freshRib, freshLeg, freshPicnic, freshShoulderButt, freshBonedRolledShoulder, nil]];
    smokedPork = [[LKMeat alloc] initWithName:@"Smoked Pork" subMeats:[NSArray arrayWithObjects:smokedShoulderPicnicHam, smokedBonelessButt2, smokedBonelessButt4, smokedHam12to20, smokedHam10, smokedHalfHam, nil]];
    
    //Initialize an LKMeat object for all types of meat.
    meat = [[LKMeat alloc] initWithName:@"Meat" subMeats:[NSArray arrayWithObjects:beef, chicken, turkey, freshPork, smokedPork, nil]];
    */
    
    //Initalize common calculate blocks to use.
    calcPerLb = ^float(float cTime, float meatWeight) {
        return meatWeight * cTime;
    };
    calcPerSide = ^float(float cTime, float meatWeight) {
        return cTime * 2;
    };
    calcTotal = ^float(float cTime, float meatWeight) {
        return cTime;
    };
    calcNone = ^float(float cTime, float meatWeight) {
        return 0;
    };
    
    
    //New meat values (gov-approved)
    LKMeat *beef01 = [[LKMeat alloc] initWithName:@"Rib Roast (bone in)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:25];
    LKMeat *beef02 = [[LKMeat alloc] initWithName:@"Rib Roast (boneless rolled)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:33];
    LKMeat *beef03 = [[LKMeat alloc] initWithName:@"Chuck Roast (Brisket)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Braise" cookTimeCalculation:calcNone cookTime:0];
    LKMeat *beef04 = [[LKMeat alloc] initWithName:@"Round/Rump Roast" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:35];
    LKMeat *beef05 = [[LKMeat alloc] initWithName:@"Tenderloin (whole)" subMeats:nil cookTempF:425 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:60];
    LKMeat *beef06 = [[LKMeat alloc] initWithName:@"Steak" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Broil/Grill" cookTimeCalculation:calcPerSide cookTime:5];
    LKMeat *beef07 = [[LKMeat alloc] initWithName:@"Stew/Shank Cross Cuts" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Simmer" cookTimeCalculation:calcTotal cookTime:180];
    LKMeat *beef08 = [[LKMeat alloc] initWithName:@"Short Ribs" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Braise" cookTimeCalculation:calcNone cookTime:0];
    LKMeat *beef09 = [[LKMeat alloc] initWithName:@"Fresh Hamburger Patties" subMeats:nil cookTempF:0 internalTempF:160 restTime:0 cookMethod:@"Broil/Grill" cookTimeCalculation:calcPerSide cookTime:5];
    
    LKMeat *beef = [[LKMeat alloc] initWithName:@"Beef" subMeats:[NSArray arrayWithObjects:beef01, beef02, beef03, beef04, beef05, beef06, beef07, beef08, beef09, nil]];
    
    //shu = smoked ham uncooked
    LKMeat *shu01 = [[LKMeat alloc] initWithName:@"Whole (bone in)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:20];
    LKMeat *shu02 = [[LKMeat alloc] initWithName:@"Half (bone in)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:25];
    LKMeat *shu03 = [[LKMeat alloc] initWithName:@"Shank/Butt portion (bone in)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:40];
    LKMeat *shu04 = [[LKMeat alloc] initWithName:@"Arm Picnic Shoulder (boneless)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:35];
    LKMeat *shu05 = [[LKMeat alloc] initWithName:@"Shoulder Roll (butt - boneless)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:40];
    
    LKMeat *shu = [[LKMeat alloc] initWithName:@"Smoked Ham (uncooked)" subMeats:[NSArray arrayWithObjects:shu01, shu02, shu03, shu04, shu05, nil]];
            
    //shc = smoked ham cooked
    LKMeat *shc01 = [[LKMeat alloc] initWithName:@"Half (bone in)" subMeats:nil cookTempF:325 internalTempF:160 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:24];
    LKMeat *shc02 = [[LKMeat alloc] initWithName:@"Arm Picnic Shoulder (boneless)" subMeats:nil cookTempF:325 internalTempF:160 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:30];
    LKMeat *shc03 = [[LKMeat alloc] initWithName:@"Canned Han (boneless)" subMeats:nil cookTempF:325 internalTempF:160 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:20];
    LKMeat *shc04 = [[LKMeat alloc] initWithName:@"Vacuum-packed (boneless)" subMeats:nil cookTempF:325 internalTempF:160 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:20];
    LKMeat *shc05 = [[LKMeat alloc] initWithName:@"Spiral cut (half or whole)" subMeats:nil cookTempF:325 internalTempF:160 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:18];
    
    LKMeat *shc = [[LKMeat alloc] initWithName:@"Smoked Ham (cooked)" subMeats:[NSArray arrayWithObjects:shc01, shc02, shc03, shc04, shc05, nil]];
    
    //fhu = fresh ham uncooked
    LKMeat *fhu01 = [[LKMeat alloc] initWithName:@"Whole Leg (bone in)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:26];
    LKMeat *fhu02 = [[LKMeat alloc] initWithName:@"Whole Leg (boneless)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:28];
    LKMeat *fhu03 = [[LKMeat alloc] initWithName:@"Hald (bone in)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:40];
    
    LKMeat *fhu = [[LKMeat alloc] initWithName:@"Fresh Ham (uncooked)" subMeats:[NSArray arrayWithObjects:fhu01, fhu02,fhu03, nil]];
    
    LKMeat *lamb01 = [[LKMeat alloc] initWithName:@"Lamb Leg (bone in - 5-7lb)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:25];
    LKMeat *lamb02 = [[LKMeat alloc] initWithName:@"Lamb Leg (bone in - 7-9lb)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:20];
    LKMeat *lamb03 = [[LKMeat alloc] initWithName:@"Lamb Leg (boneless - rolled)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:30];
    LKMeat *lamb04 = [[LKMeat alloc] initWithName:@"Shoulder Roast/Shank Leg Half" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:35];
    LKMeat *lamb05 = [[LKMeat alloc] initWithName:@"Cubes (for kabobs)" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Broil/Grill" cookTimeCalculation:calcTotal cookTime:12];
    LKMeat *lamb06 = [[LKMeat alloc] initWithName:@"Ground Lamb Patties" subMeats:nil cookTempF:0 internalTempF:160 restTime:0 cookMethod:@"Broil/Grill" cookTimeCalculation:calcTotal cookTime:8];
    LKMeat *lamb07 = [[LKMeat alloc] initWithName:@"Chops/Ribs/Loins" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Broil/Grill" cookTimeCalculation:calcTotal cookTime:11];
    LKMeat *lamb08 = [[LKMeat alloc] initWithName:@"Leg Steaks" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Broil/Grill" cookTimeCalculation:calcTotal cookTime:18];
    LKMeat *lamb09 = [[LKMeat alloc] initWithName:@"Stew Meats (pieces)" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Simmer" cookTimeCalculation:calcTotal cookTime:120];
    LKMeat *lamb10 = [[LKMeat alloc] initWithName:@"Shanks" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Simmer" cookTimeCalculation:calcTotal cookTime:120];
    LKMeat *lamb11 = [[LKMeat alloc] initWithName:@"Breast (rolled)" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Braise" cookTimeCalculation:calcTotal cookTime:120];
    
    LKMeat *lamb = [[LKMeat alloc] initWithName:@"Lamb" subMeats:[NSArray arrayWithObjects:lamb01, lamb02, lamb03, lamb04, lamb05, lamb06, lamb07, lamb08, lamb09, lamb10, lamb11, nil]];
    
    LKMeat *pork01 = [[LKMeat alloc] initWithName:@"Loin Roast (bone in/boneless)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:30];
    LKMeat *pork02 = [[LKMeat alloc] initWithName:@"Crown Roast" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:30];
    LKMeat *pork03 = [[LKMeat alloc] initWithName:@"Leg (Fresh Ham) Whole (bone in)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:26];
    LKMeat *pork04 = [[LKMeat alloc] initWithName:@"Leg (Fresh Ham) Half (bone in)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:40];
    LKMeat *pork05 = [[LKMeat alloc] initWithName:@"Boston Butt" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:45];
    LKMeat *pork06 = [[LKMeat alloc] initWithName:@"Tenderloin" subMeats:nil cookTempF:450 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:30];
    LKMeat *pork07 = [[LKMeat alloc] initWithName:@"Ribs (back/country-style/Spareribs)" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:120];
    LKMeat *pork08 = [[LKMeat alloc] initWithName:@"Loin Chops (bone in/boneless)" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Broil/Grill" cookTimeCalculation:calcTotal cookTime:16];
    LKMeat *pork09 = [[LKMeat alloc] initWithName:@"Ground Pork Patties" subMeats:nil cookTempF:0 internalTempF:160 restTime:0 cookMethod:@"Skillet" cookTimeCalculation:calcTotal cookTime:10];
    LKMeat *pork10 = [[LKMeat alloc] initWithName:@"Loin Chops/Cutlets (1/4in thick)" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Skillet" cookTimeCalculation:calcTotal cookTime:4];
    LKMeat *pork11 = [[LKMeat alloc] initWithName:@"Loin Chops/Cutlets (3/4in thick)" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Skillet" cookTimeCalculation:calcTotal cookTime:8];
    LKMeat *pork12 = [[LKMeat alloc] initWithName:@"Tenderloin Medallions" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Skillet" cookTimeCalculation:calcTotal cookTime:8];
    
    LKMeat *pork = [[LKMeat alloc] initWithName:@"Pork" subMeats:[NSArray arrayWithObjects:pork01, pork02, pork03, pork04, pork05, pork06, pork07, pork08, pork09, pork10, pork11, pork12, nil]];
    
    LKMeat *veal01 = [[LKMeat alloc] initWithName:@"Rib Roast" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:27];
    LKMeat *veal02 = [[LKMeat alloc] initWithName:@"Loin" subMeats:nil cookTempF:325 internalTempF:145 restTime:3 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:36];
    LKMeat *veal03 = [[LKMeat alloc] initWithName:@"Loin/Rib Chops" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Broil/Grill" cookTimeCalculation:calcPerSide cookTime:7];
    LKMeat *veal04 = [[LKMeat alloc] initWithName:@"Cutlets (1/8\" thick)" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Pan Fry" cookTimeCalculation:calcTotal cookTime:4];
    LKMeat *veal05 = [[LKMeat alloc] initWithName:@"Cutlets (1/4\" thick)" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Pan Fry" cookTimeCalculation:calcTotal cookTime:6];
    LKMeat *veal06 = [[LKMeat alloc] initWithName:@"Arm/Blade Steak" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Broil/Grill" cookTimeCalculation:calcPerSide cookTime:8];
    LKMeat *veal07 = [[LKMeat alloc] initWithName:@"Round Steak (1/4\" thick)" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Braise" cookTimeCalculation:calcTotal cookTime:30];
    LKMeat *veal08 = [[LKMeat alloc] initWithName:@"Round Steak (1/2\" thick)" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Braise" cookTimeCalculation:calcTotal cookTime:45];
    LKMeat *veal09 = [[LKMeat alloc] initWithName:@"Boneless Breast (stuffed - 2-2.5lb)" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Braise" cookTimeCalculation:calcTotal cookTime:90];
    LKMeat *veal10 = [[LKMeat alloc] initWithName:@"Boneless Breast (stuffed - 4-4.5lb)" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Braise" cookTimeCalculation:calcTotal cookTime:150];
    LKMeat *veal11 = [[LKMeat alloc] initWithName:@"Cross Cut Shanks" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Simmer" cookTimeCalculation:calcTotal cookTime:75];
    LKMeat *veal12 = [[LKMeat alloc] initWithName:@"Stew Meats" subMeats:nil cookTempF:0 internalTempF:145 restTime:3 cookMethod:@"Simmer" cookTimeCalculation:calcTotal cookTime:60];
    LKMeat *veal13 = [[LKMeat alloc] initWithName:@"Ground Patties" subMeats:nil cookTempF:0 internalTempF:160 restTime:0 cookMethod:@"Broil/Grill" cookTimeCalculation:calcPerSide cookTime:7];
    
    LKMeat *veal = [[LKMeat alloc] initWithName:@"Veal" subMeats:[NSArray arrayWithObjects:veal01, veal02, veal03, veal04, veal05, veal06, veal07, veal08, veal09, veal10, veal11, veal12, veal13, nil]];
    
    //turk = turkey
    LKMeat *turk01 = [[LKMeat alloc] initWithName:@"Unstuffed (breast - 4-6lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:20 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:105];
    LKMeat *turk02 = [[LKMeat alloc] initWithName:@"Unstuffed (breast - 6-8lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:20 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:165];
    LKMeat *turk03 = [[LKMeat alloc] initWithName:@"Unstuffed (8-12lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:20 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:180];
    LKMeat *turk04 = [[LKMeat alloc] initWithName:@"Unstuffed (12-14lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:20 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:195];
    LKMeat *turk05 = [[LKMeat alloc] initWithName:@"Unstuffed (14-18lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:20 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:240];
    LKMeat *turk06 = [[LKMeat alloc] initWithName:@"Unstuffed (18-20lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:20 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:270];
    LKMeat *turk07 = [[LKMeat alloc] initWithName:@"Unstuffed (20-24lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:20 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:315];
    LKMeat *turk08 = [[LKMeat alloc] initWithName:@"Stuffed (8-12lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:20 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:135];
    LKMeat *turk09 = [[LKMeat alloc] initWithName:@"Stuffed (12-14lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:20 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:225];
    LKMeat *turk10 = [[LKMeat alloc] initWithName:@"Stuffed (14-18lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:20 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:240];
    LKMeat *turk11 = [[LKMeat alloc] initWithName:@"Stuffed (18-20lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:20 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:270];
    LKMeat *turk12 = [[LKMeat alloc] initWithName:@"Stuffed (20-24lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:20 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:300];
    
    LKMeat *turk = [[LKMeat alloc] initWithName:@"Turkey" subMeats:[NSArray arrayWithObjects:turk01, turk02, turk03, turk04, turk05, turk06, turk07, turk08, turk09, turk10, turk11, turk12, nil]];
    
    //chick = chicken
    LKMeat *chick01 = [[LKMeat alloc] initWithName:@"Whole Broiler - Fryer (Unstuffed)" subMeats:nil cookTempF:350 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:90];
    LKMeat *chick02 = [[LKMeat alloc] initWithName:@"Whole Roasting Hen (Unstuffed)" subMeats:nil cookTempF:350 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:120];
    LKMeat *chick03 = [[LKMeat alloc] initWithName:@"Whole Capon (Unstuffed)" subMeats:nil cookTempF:350 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:150];
    LKMeat *chick04 = [[LKMeat alloc] initWithName:@"Whole Cornish Hen (Unstuffed)" subMeats:nil cookTempF:350 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:60];
    LKMeat *chick05 = [[LKMeat alloc] initWithName:@"Breash Halves (bone in)" subMeats:nil cookTempF:350 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:35];
    LKMeat *chick06 = [[LKMeat alloc] initWithName:@"Breast Half (boneless)" subMeats:nil cookTempF:350 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:25];
    LKMeat *chick07 = [[LKMeat alloc] initWithName:@"Legs/Thighs" subMeats:nil cookTempF:350 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:45];
    LKMeat *chick08 = [[LKMeat alloc] initWithName:@"Drumsticks" subMeats:nil cookTempF:350 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:40];
    LKMeat *chick09 = [[LKMeat alloc] initWithName:@"Wings/Wingettes" subMeats:nil cookTempF:350 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:35];
    
    LKMeat *chick = [[LKMeat alloc] initWithName:@"Chicken" subMeats:[NSArray arrayWithObjects:chick01, chick02, chick03, chick04, chick05, chick06, chick07, chick08, chick09, nil]];
    
    LKMeat *duck01 = [[LKMeat alloc] initWithName:@"Whole Duckling (Unstuffed)" subMeats:nil cookTempF:350 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcPerLb cookTime:35];
    LKMeat *duck02 = [[LKMeat alloc] initWithName:@"Duckling Breast" subMeats:nil cookTempF:0 internalTempF:165 restTime:0 cookMethod:@"Braise" cookTimeCalculation:calcTotal cookTime:70];
    LKMeat *duck03 = [[LKMeat alloc] initWithName:@"Duckling Legs/Thighs" subMeats:nil cookTempF:325 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:90];
    
    LKMeat *duck = [[LKMeat alloc] initWithName:@"Duck" subMeats:[NSArray arrayWithObjects:duck01, duck02, duck03, nil]];
    
    LKMeat *goose01 = [[LKMeat alloc] initWithName:@"Whole Young Goose (Unstuffed - 8-12lb)" subMeats:nil cookTempF:325 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:165];
    LKMeat *goose02 = [[LKMeat alloc] initWithName:@"Young Goose (cut up)" subMeats:nil cookTempF:325 internalTempF:165 restTime:0 cookMethod:@"Roast" cookTimeCalculation:calcTotal cookTime:120];
    
    LKMeat *goose = [[LKMeat alloc] initWithName:@"Goose" subMeats:[NSArray arrayWithObjects:goose01, goose02, nil]];
    
    meat = [[LKMeat alloc] initWithName:@"Meat" subMeats:[NSArray arrayWithObjects: beef, shu, shc, fhu, lamb, pork, veal, turk, chick, duck, goose, nil]];
     
    //Finally, initialize an LKMeat object to contain the meat that's selected.
    currentMeatSelection = [[LKMeat alloc] init];
    currentSubMeatSelection = [[LKMeat alloc] init];
    
    //Initialize the LKMeat objects to the first meat, so its not 0.
    currentMeatSelection = [meat.subMeats objectAtIndex:0];
    currentSubMeatSelection = [currentMeatSelection.subMeats objectAtIndex:0];
    
    //After initialization, check for first launch.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL before = [defaults boolForKey:@"MeatCalcPrevLaunch"];
    if (!before) {
        //Display a greeting message to the user
        NSString *introMsg = @"Welcome to Meat Calculator! This app prevents you from undercooking your meat by telling you for how long and at what temperature to cook it at. Many diseases can be prevented with this simple tool! To use it, just select the meat you are cooking. Enter the weight (in pounds). Press \"Calculate\" and you're done! The information you need will be on the top right of the screen. For more information, press the \"i\" on the top left of the screen.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Welcome!" message:introMsg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        //Set the value to true so that the message is only displayed once.
        [defaults setBool:YES forKey:@"MeatCalcPrevLaunch"];
    }
    
    //Initialize the flipside view controller.
    flipside = [[FlipsideViewController alloc] initWithNibName:@"FlipsideViewController" bundle:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //interface orientation for iPhone
        return (UIInterfaceOrientationIsLandscape(interfaceOrientation));
    } else {
        //interface orientation for iPad
        return (UIInterfaceOrientationIsLandscape(interfaceOrientation));
    }
}
#pragma mark - iPad Only

- (IBAction)timerstart:(id)sender
{
    minRemaining = [remainingTimeLabel.text intValue];
    progressView.progress = 0.0;
    
    timer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
    [startTimeButton setTitle:@"Running..." forState:UIControlStateNormal];
    startTimeButton.enabled = FALSE;
    startTimeButton.alpha = 0.5;
}
- (IBAction)timerAction:(id)sender
{
    minRemaining--;
    
    float timerProgress = ((float)cookTime - (float)minRemaining) / (float)cookTime;
    progressView.progress = timerProgress;
        
    remainingTimeLabel.text = [[NSString alloc] initWithFormat:@"%d", minRemaining];
    
    if (minRemaining < 1){
        
        [timer invalidate];
        timer = Nil;
        
        NSURL *TimeDonePath = [[NSURL alloc] initFileURLWithPath:[[NSBundle mainBundle] pathForResource:@"alarm_clock_bell_ringing" ofType:@"mp3"]];
        AVAudioPlayer *player1 = [[AVAudioPlayer alloc]initWithContentsOfURL:TimeDonePath error:NULL];
        player1.volume = .75;
        [player1 prepareToPlay];
        [player1 play];
        [startTimeButton setTitle:@"Start" forState:UIControlStateNormal];
        startTimeButton.enabled = TRUE;
        startTimeButton.alpha = 1.0;
    }


    
}

#pragma mark - Flipside View Controller

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissModalViewControllerAnimated:YES];
    } else {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
    }
}

- (IBAction)showInfo:(id)sender
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        flipside.delegate = self;
        flipside.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentModalViewController:flipside animated:YES];
    } else {
        if (!self.flipsidePopoverController) {
            flipside.delegate = self;
            
            self.flipsidePopoverController = [[UIPopoverController alloc] initWithContentViewController:flipside];
        }
        if ([self.flipsidePopoverController isPopoverVisible]) {
            [self.flipsidePopoverController dismissPopoverAnimated:YES];
        } else {
            self.flipsidePopoverController.popoverContentSize = flipside.view.frame.size;
            [self.flipsidePopoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
}

@end
